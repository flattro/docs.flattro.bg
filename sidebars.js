module.exports = {
  someSidebar: {
    'Обща информация': [
        'about',
    ],
    'Управление на имоти' : [
        'property1',
        'property2',
        'property3',
        'property4',
        'property5'
    ],
    'Управление на комплекси': [
        'facility1',
        'facility2',
        {
            Единици: [
                'unit1',
                'unit2',
                'unit3',
                'unit4',
                'unit5'
            ]
        },
        {
            Собственици: [
                'owner1',
                'owner2',
                'owner3',
                'owner4',
                'owner5'
            ]
        },
        'facility5',
        'facility6',
        'facility7'
    ],
    'Основни функции': [
        // 'addExpense',
        // 'addRevenue',
        'collectionHistory',
        'transactionHistory',
        'profile'
    ],
    // 'Други': [
    //     'example'
    // ]
  },
};

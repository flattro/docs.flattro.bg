---
id: unit5
title: Настройки
---

## Tаксуване
Сумата, която виждате в полето **такса мениджмънт**, е дължимата месечна такса на единицата в лева.

### Коефициенти
Тук можете да редактирате стойността на коефициентите, които се използват за изчисляване на периодичните такси, които са дължими за комплекса. Периодичните такси за единицата се изчисляват по следната формула: **стойност на коефициента Х брой месеци Х мултипликатор на коефициента**. Броят месеци и мултипликаторът на коефициента се настройват в съответния комплекс.

:::note Пример
Единица с площ **50 кв.м. (стойност на коефициента)** е част от комплекс с **тримесечно начисляване (3 броя месеци)** и размер на такса управление **(мултипликатор на коефициента) от 0,50 лв**. На всяко тримесечие системата ще начислява такса управление в размер на **50 х 3 х 0,50 = 75,00 лв.**
:::

#### Временна промяна на коефициент
Ако за определена единица трябва инциндентно да се начисли различен от стандартния размер периодична такса (например продажба на единицата и добавянето на нов собственик, който трябва да заплати частична такса за периода), трябва да промените коефициента на единицата, след което да начислите повтарящата се такса за съответния период.

:::note Пример
Единица с площ **50 кв.м. (стойност на коефициента)** е част от комплекс с **тримесечно начисляване (3 броя месеци)** и размер на такса управление **(мултипликатор на коефициента) от 0,50 лв**. Ако желаете да се начисли такса само за **2 месеца**, изчислявате нужния временен коефициент по следния начин: **(инцидентен период / стандартен период) Х стандартна стойност на коефициента** или **_(2 / 3) х 50 = 33,33_**. При начисляване на таксите за периода вместо стандартния размер от 75,00 лв. ще бъдат начислени **50,00 лв. (33,33 х 3 х 0,50)**.
:::

:::warning Внимание!
Не забравяйте да върнете реалния коефициент след начисляването, за да е коректно изчисляването на следващата периодична такса.
:::

## Оперативни настройки
Можете да редактирате името на единицата и всички останали опции.

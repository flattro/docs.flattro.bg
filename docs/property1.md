---
id: property1
title: Работа с имоти
---

import useBaseUrl from '@docusaurus/useBaseUrl';

На основната страница с имоти можете да разгледате списък с всички имоти, които притежавате и/или управлявате. В списъка виждате наименованието на съответния имот, както и общия депозит към момента, който може да бъде положителен или отрицателен (само за управлявани имоти).

<img alt="Списък с имоти" src={useBaseUrl('img/properties_list.png')} width={339}/>

За да влезете в избрания имот, кликнете върху името на имота. Друг вариант за достигане до съответния имот (когато същият се намира в комплекс, който Вие управлявате) е през съответната единица в комплекса чрез бутона **`Управлявай имота`**.

:::note За мениджъри
Над списъка с имоти можете да разгледате статистика за средния размер на баланса на всички управлявани от Вас имоти.
:::

:::success Знаете ли, че...?
Всеки имот може да бъде свързан с неограничен брой единици. По този начин собственикът или мениджърът на имота може да групира единиците, свързани с имота, по удобен за него начин. Например, в един имот може да се групират апартамент, гараж, паркомясто и мазе. А ако съответните доставчици на електроенергия, топлоенергия и ВиК услуги използват Flattro, можете да добавите и съответните клиентски номера, за да следите автоматично и разплащате удобно битовите разходи на имота. 
:::

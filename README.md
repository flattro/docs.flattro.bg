# Flattro Docs

Flattro Documentation is built using [Docusaurus 2](https://v2.docusaurus.io/). If you'd like to contribute, please, clone the repo and follow the instructions below as well as Docusaurus 2 documentation to make changes. Currently, we only support Bulgarian language.

## Installation

```console
yarn install
```

## Local Development

```console
yarn start
```

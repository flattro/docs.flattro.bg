import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: 'Лесно използване',
    imageUrl: 'img/S2_Social_Networking.svg',
    description: (
      <>
        Вече няма нужда да жонглирате с десетки ексел файлове.
        Всичко е на един бутон и няколко клика разстояние.
      </>
    ),
  },
  {
    title: 'Фокус върху важните неща',
    imageUrl: 'img/S2_Dashboard.svg',
    description: (
      <>
        Flattro Ви позволява да се фокусирате върху важните неща,
        докато се грижи за Важите ежедневни задачи и комуникация.
      </>
    ),
  },
  {
    title: 'Работете от всякъде',
    imageUrl: 'img/S2_Girl_Working.svg',
    description: (
      <>
        С Flattro няма нужда да сте на бюрото си, за да свършите важните неща.
        Работете по-ефективно и лесно от всякъде!
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={
        //   `Документация на ${siteConfig.title}`
          'Документация за работа с Flattro'
        }
      description="Всичко, от което се нуждаете, за да започнете работа с Flattro!">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/general/')}>
              Да започваме
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;

module.exports = {
  title: 'Flattro Docs',
  tagline: 'Документация за работа с Flattro',
  url: 'https://docs.flattro.bg',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  favicon: 'img/favicon.png',
  organizationName: 'flattro', // Usually your GitHub org/user name.
  projectName: 'docs.flattro.bg', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Flattro Docs',
      logo: {
        alt: 'Flattro',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/general/',
          activeBasePath: 'docs',
          label: 'Документация',
          position: 'left',
        },
        // {to: 'blog', label: 'Blog', position: 'left'},
        // {
        //   href: 'https://github.com/facebook/docusaurus',
        //   label: 'GitHub',
        //   position: 'right',
        // },
        {
          type: 'localeDropdown',
          position: 'left',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Документация',
          items: [
            {
              label: 'Обща информация',
              to: 'docs/general/',
            },
            {
              label: 'Управление на имоти',
              to: 'docs/property1/',
            },
            {
              label: 'Управление на комплекси',
              to: 'docs/facility1/',
            },
          ],
        },
        {
          title: 'Общност',
          items: [
            {
              label: 'Facebook',
              href: 'https://stackoverflow.com/questions/tagged/docusaurus',
            },
            {
              label: 'Twitter',
              href: 'https://discordapp.com/invite/docusaurus',
            },
          ],
        },
        {
          title: 'Още',
          items: [
            {
              label: 'flattro.bg',
              to: 'https://flattro.bg',
            },
            {
              label: 'Bitbucket',
              href: 'https://bitbucket.org/elev8or/docs.flattro.bg',
            },
          ],
        },
      ],
      copyright: `Всички права запазени © ${new Date().getFullYear()} Flattro`,
    },
    algolia: {
        appId: 'IWUI5JF96C',
        // apiKey: '9c7c3b3c55c0f06624c6ccd3a5e0fbb5',
      indexName: 'flattro',
      apiKey: '0837335b0157645450ed5e12e462dff1',

        // Optional: see doc section bellow
        contextualSearch: true,

        // Optional: Algolia search parameters
        searchParameters: {
            'facetFilters': ["type:content", "version:current"]
        },

        //... other Algolia params
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
        //   editUrl:
        //     'https://bitbucket.org/elev8or/docs.flattro.bg/src/master/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
        //   editUrl:
        //     'https://bitbucket.org/elev8or/docs.flattro.bg/src/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  i18n: {
    defaultLocale: 'bg',
    locales: ['bg'],
  },
};
